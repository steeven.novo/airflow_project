import requests
import json
from datetime import datetime
from datetime import timedelta
from pymongo import MongoClient 
import airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator


def get_data():    
    # GET DATA
    apiKey = "dc4891b16106caf47ff409f10ea1ee21"
    urlProfile = "https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=" + apiKey
    urlRating = "https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=" + apiKey
    profileRequest = requests.get(urlProfile)
    ratingRequest = requests.get(urlRating)

    # CREATE THE DICTIONNARY
    data = {}
    data['profile'] = profileRequest.json()
    data['rating'] = ratingRequest.json()
    data['timestamp'] = datetime.now().timestamp()

    with open("/tmp/json_file_data", 'w') as json_file:
        json.dump(data, json_file, indent=4)


def insert_data():
    with open("/tmp/json_file_data", 'r') as json_file:
        data = json.load(json_file)
    # INSERT DATA IN MONGODB
    client = MongoClient("mongodb://mongodb:27017/")
    db = client['mydb']
    collection = db['collection']
    collection.insert_one(data)




# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'adhoc':False,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'trigger_rule': u'all_success'
}

dag = DAG(
    'projet',
    default_args=default_args,
    description='Get and insert data',
    schedule_interval=timedelta(minutes=2),
)

t1 = PythonOperator(
    task_id="get_data",
    python_callable=get_data,
    dag=dag,
)

t2 = PythonOperator(
    task_id="insert_data",
    python_callable=insert_data,
    dag=dag,
)

t1 >> t2